process.title = 'client-app';

var WebSocket = require('websocket').w3cwebsocket;
var keypress = require('keypress');
 
// make `process.stdin` begin emitting "keypress" events
keypress(process.stdin);

var connection = new WebSocket('ws://localhost:3200');

connection.onopen = function () {
  // connection is opened and ready to use
  console.log('##### '+(new Date())+' #####');
  console.log('Welcome to WebSocket Game!\nLet\'s play the Game.');
};

connection.onerror = function (error) {
    // an error occurred when sending/receiving data
    console.log('Sorry, but there\'s some problem with your connection or the server is down.');
};

connection.onmessage = function (message) {
    // try to decode json (I assume that each message
    // from server is json)
    try {
      var json = JSON.parse(message.data);
    } catch (e) {
      console.log('This doesn\'t look like a valid JSON: ',
          message.data);
      return;
    }
    // handle incoming message
    if (json.type === 'instruction') {
      // receive instruction with timeout
      console.log('instruction : '+json.data.instruction+' , timeout : '+json.data.timeout+'seconds');
      console.log('Press same key within timeout');
    }
    else if(json.type === 'result') {
      console.log('Your Score : ' + json.score);
    }
    else if(json.type === 'over') {
      console.log('Game Over! Your Score : ' + json.score);
    }
};

// listen for the "keypress" event
process.stdin.on('keypress', function (ch, key) {
  console.log('keypressed : ', key.name);
  if (key && key.ctrl && key.name == 'c') {
    process.exit(1);
  }
  // broadcast message to server
  connection.send(key.name);
});
process.stdin.setRawMode(true);
process.stdin.resume();