# WebsocketGame

A follow-me like game using web-socket

Steps to run the application
1. In cli, go to server-app folder and run "npm install"
2. Then to start server, run "node app.js"
3. Again in another cli, go to client-app folder and run "npm install"
4. Then to start client, run "node app.js"
5. Now you can check communication between client and server over websocket by pressing keys to play the game
6. To exit from either server or client, press "ctr + c"

![Server App](./server.JPG)
![Client App](./client.JPG)