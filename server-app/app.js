process.title = 'server-app';

var WebSocketServer = require('websocket').server;
var http = require('http');
var keypress = require('keypress');
 
// make `process.stdin` begin emitting "keypress" events
keypress(process.stdin);

/**
 * Global variables
 */
var timer = 10;
var score = 0;
var instruction = '';
var isTimeout = true;
var timeoutObj = null;
var noResponse = 0;
// list of currently connected clients
var clients = [ ];

var server = http.createServer(function(request, response) {
  // process HTTP request. Since we're writing just WebSockets
  // server we don't have to implement anything.
});
server.listen(3200, function() {
    console.log('##### '+(new Date())+' #####');
    console.log("Server is listening on port : 3200");
});

/**
 * WebSocket server
 */
wsServer = new WebSocketServer({
  httpServer: server
});

// WebSocket server
wsServer.on('request', function(request) {
    console.log('Connection requested.');
    var connection = request.accept(null, request.origin);
    // Add client to list
    var index = clients.push(connection) - 1;

    console.log('Connection accepted.');
    console.log('Client is ready! Let\'s start the Game.');

    connection.on('message', function(message) {

        if(message)
        {
            console.log('Received : ' + message.utf8Data);
            if(!isTimeout)
            {
                noResponse = 0;
                if(instruction === message.utf8Data)
                {
                    score++;
                }
                else
                {
                    score--;
                }

                if(score==10 || score==-3)
                {
                    // broadcast message to connected client
                    var json = JSON.stringify({ type:'over', score: score });
                    connection.sendUTF(json);
                    console.log('Game Over');
                    score = 0;
                }
                else
                {
                    // broadcast message to connected client
                    var json = JSON.stringify({ type:'result', score: score });
                    connection.sendUTF(json);
                }
                isTimeout = true;
                clearTimeout(timeoutObj);
            }
        }
    });

    connection.on('close', function(connection) {
        // close user connection
        console.log("Client disconnected.");
        // remove user from the list of connected clients
        clients.splice(index, 1);
    });

    // listen for the "keypress" event
    process.stdin.on('keypress', function (ch, key) {
        console.log('keypressed : ', key.name);
        if (key && key.ctrl && key.name == 'c') {
            process.exit(1);
        }
        instruction = key.name;
        // broadcast message to connected client
        var json = JSON.stringify({ type:'instruction', data: {instruction: key.name, timeout: timer} });
        connection.sendUTF(json);

        isTimeout = false;
        timeoutObj = setTimeout(function () {
            if(!isTimeout)
            {
                console.log('Timeout : '+timer+'seconds');
                isTimeout = true;
                noResponse++;
                if(noResponse==3)
                {
                    // broadcast message to connected client
                    var json = JSON.stringify({ type:'over', score: score });
                    connection.sendUTF(json);
                    console.log('Game Over');
                    score = 0;
                    noResponse = 0;
                }
            }
        }, timer*1000);
    });
});

process.stdin.setRawMode(true);
process.stdin.resume();